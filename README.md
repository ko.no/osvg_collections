# Shape Collection for Object-oriented Scalable Vector Graphics (osvg)

## Table of Content

- [Installation](#installation)
- [Major Goal](#major-goal)
- [Collection Overview](#collection-overview)
  - [it](#it---information-technology)
    - [network](#network---network-elements)

## Installation

```code
pip install osvg_collection
```

## Major Goal

Provide collections of standard shapes to easily create SVG drawings based on osvg module.

## Collection Overview

### it - *Information Technology*

#### network - *Network Elements*

- cloud - *Cloud element*

  ![Cloud Shapes](test/system/it/network/clouds.svg)
- firewall - *Firewall element*

  ![Firewall Shapes](test/system/it/network/firewalls.svg)
- router - *Router element plus different 'colored' sub-classes*

  ![Router Shapes](test/system/it/network/routers.svg)
- switch - *Switch element plus different 'colored' sub-classes*

  ![Switch Shapes](test/system/it/network/switches.svg)
