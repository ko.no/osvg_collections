# OSVG Collections Release Notes

## v1.4.0

Update:

- generic.connectors: 2P Connector as one Polyline SVG element to
  support stroke style attributes

## v1.3.1

Update:

- generic.connectors: Internal adaption of upstream osvg changes in v2.1.0

## v1.3.0

Update:

- Support python module osvg version 2

## v1.2.0

Updated Elements/Shapes:

- generic.connector: MPConnector
  - add method: add_position
  - add average option

## v1.1.0

New Elements/Shapes:

- Category **generic.connector** added with these elements:
  - **ConnectorH**: Horizontal Two-Point Connector
  - **ConnectorV**: Vertical Two-Point Connector
  - **MPConnectorH**: Horizontal Multi-Point Connector
  - **MPConnectorV**: Vertical Multi-Point Connector

## v1.0.0

Initial release.
