"""
Python script to update release information
from given git tag.
"""

import sys
import os

MODUL_PATH = "osvg_collections"
VERSION_FORMAT = "v<major integer>.<minor integer>.<patch integer>"


def replace_version_string(content: str, version: tuple[int, int, int]) -> str:
    """
    Replaces version string with new version string and return the content
    """
    return content.replace("0.0.999", f"{version[0]}.{version[1]}.{version[2]}")


def replace_version_tuple_string(content: str, version: tuple[int, int, int]) -> str:
    """
    Replaces version tuple string with new version tuple string and return the content
    """
    return content.replace("(0, 0, 999)", f"({version[0]}, {version[1]}, {version[2]})")


def parse_args() -> tuple[int, int, int]:
    """
    Verify and parse arguments.
    """
    if len(sys.argv) < 2:
        print(f"Missing version string '{VERSION_FORMAT}' missing.")
        sys.exit(1)
    elif len(sys.argv) > 2:
        print("Too many arguments. Only one version string allowed.")
        sys.exit(1)
    ver_str = sys.argv[1]
    if ver_str[0].lower() != "v":
        print("Version string must start with 'v' or 'V'")
        sys.exit(1)
    try:
        major, minor, patch = ver_str[1:].split(".")
        major = int(major)
        minor = int(minor)
        patch = int(patch)
    except ValueError:
        print(f"Version string must be of format '{VERSION_FORMAT}'")
        sys.exit(1)
    return major, minor, patch


def update_init_py(version: tuple[int, int, int]) -> None:
    """
    Update version information in osvg_ollections.__init__.py
    """
    init_file_name = "__init__.py"
    with open(os.path.join(MODUL_PATH, init_file_name), "r", encoding="UTF-8") as f:
        file_content = f.read()
    file_content = replace_version_string(content=file_content, version=version)
    file_content = replace_version_tuple_string(content=file_content, version=version)
    with open(os.path.join(MODUL_PATH, init_file_name), "w", encoding="UTF-8") as f:
        f.write(file_content)


def update_pyproject_toml(version: tuple[int, int, int]) -> None:
    """
    Update version information in pyproject.toml
    """
    toml_file_name = "pyproject.toml"
    with open(toml_file_name, "r", encoding="UTF-8") as f:
        file_content = f.read()
    file_content = replace_version_string(content=file_content, version=version)
    with open(toml_file_name, "w", encoding="UTF-8") as f:
        f.write(file_content)


def check_change_log(version: tuple[int, int, int]):
    """
    Verify if a chapter for this version is in the release notes.
    """
    release_notes_file = "CHANGELOG.md"
    with open(release_notes_file, "r", encoding="UTF-8") as f:
        file_content = f.read()
    if f"## v{version[0]}.{version[1]}.{version[2]}" not in file_content:
        print(f"File {release_notes_file} does not contain a chapter for this release.")
        sys.exit(1)


def main():
    """
    Verify version string and update documentation accordingly.
    """
    version = parse_args()
    check_change_log(version=version)
    print(
        f"Updating release information for version v{version[0]}.{version[1]}.{version[2]}"
    )
    update_init_py(version=version)
    update_pyproject_toml(version=version)
    sys.exit(0)


if __name__ == "__main__":
    main()
