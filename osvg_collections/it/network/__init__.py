"""
IT/Network Collections
"""

from .switch import Switch, SwitchRed, SwitchGray, SwitchGreen, SwitchBlue, SwitchOrange
from .router import Router, RouterRed, RouterGray, RouterGreen, RouterBlue, RouterOrange
from .firewall import Firewall
from .cloud import Cloud
