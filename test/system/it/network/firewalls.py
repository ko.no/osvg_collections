"""
Creating SVG object with firewall shapes
"""

# pylint: disable=duplicate-code

import osvg
import osvg_collections.it.network


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(width=180, height=160)
    osvg.Rectangle(
        parent=svg,
        width=1000,
        height=600,
        style=osvg.Style(stroke_width=0, fill_color="e0e0e0"),
    )
    osvg_collections.it.network.Firewall(
        parent=svg,
        name="standard vertical",
        position=(25, 50),
        width=50,
        height=100,
    )
    osvg_collections.it.network.Firewall(
        parent=svg,
        name="standard horizontal",
        position=(110, 45),
        width=100,
        height=50,
        rotation=20,
    )
    osvg_collections.it.network.Firewall(
        parent=svg,
        name="standard horizontal",
        position=(110, 125),
        width=50,
        height=50,
        border_color="ff0000",
        fill_color="444444",
        joints_color="000000",
    )
    return svg
