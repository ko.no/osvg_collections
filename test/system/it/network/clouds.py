"""
Creating SVG object with cloud shapes
"""

# pylint: disable=duplicate-code

import osvg
import osvg_collections.it.network


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(width=200, height=160)
    frame_style = osvg.Style(
        fill_color="none", stroke_color="0000ff", stroke_dasharray=[1, 1]
    )
    cloud_style = osvg.Style(fill_color="444444", stroke_color="ff0000")
    osvg.Rectangle(
        parent=svg,
        width=1000,
        height=600,
        style=osvg.Style(stroke_width=0, fill_color="e0e0e0"),
    )
    osvg.Rectangle(
        parent=svg,
        position=(60, 25),
        width=100,
        height=50,
        style=frame_style,
    )
    osvg_collections.it.network.Cloud(
        parent=svg,
        name="standard horizontal",
        position=(60, 25),
        width=100,
        height=50,
        style=cloud_style,
    )
    osvg.Rectangle(
        parent=svg,
        position=(160, 25),
        width=50,
        height=10,
        style=frame_style,
    )
    osvg_collections.it.network.Cloud(
        parent=svg,
        name="flat horizontal",
        position=(160, 25),
        width=50,
        height=10,
        style=cloud_style,
    )
    osvg.Rectangle(
        parent=svg,
        position=(50, 85),
        width=50,
        height=50,
        style=frame_style,
    )
    osvg_collections.it.network.Cloud(
        parent=svg,
        name="standard square",
        position=(50, 85),
        width=50,
        height=50,
        style=cloud_style,
    )
    flat_v = osvg.Group(parent=svg, position=(110, 100), rotation=20)
    osvg.Rectangle(
        parent=flat_v,
        width=10,
        height=50,
        style=frame_style,
    )
    osvg_collections.it.network.Cloud(
        parent=flat_v,
        name="flat vertical",
        width=10,
        height=50,
        style=cloud_style,
    )
    osvg.Rectangle(
        parent=svg,
        position=(160, 100),
        width=50,
        height=100,
        style=frame_style,
    )
    osvg_collections.it.network.Cloud(
        parent=svg,
        name="standard vertical",
        position=(160, 100),
        width=50,
        height=100,
        style=cloud_style,
    )
    return svg
