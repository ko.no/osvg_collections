"""
Creating SVG object with router shapes
"""

# pylint: disable=duplicate-code

import osvg
import osvg_collections.it.network


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(width=750, height=550)
    osvg.Rectangle(
        parent=svg,
        width=750,
        height=550,
        style=osvg.Style(stroke_width=0, fill_color="ffffff"),
    )
    osvg_collections.it.network.Router(
        parent=svg,
        name="manual horizontal",
        position=(75, 25),
        width=150,
        height=50,
        border_color="ff0000",
        fill_color="444444",
        ornament_color="888888",
    )
    osvg_collections.it.network.Router(
        parent=svg,
        name="manual vertical",
        position=(25, 135),
        width=50,
        height=150,
        border_color="ff0000",
        fill_color="444444",
        ornament_color="888888",
    )
    osvg_collections.it.network.RouterRed(
        parent=svg,
        name="red horizontal",
        position=(120, 85),
        width=110,
        height=50,
    )
    osvg_collections.it.network.RouterGray(
        parent=svg,
        name="gray vertical",
        position=(120, 175),
        width=50,
        height=110,
        rotation=45,
    )
    osvg_collections.it.network.RouterGreen(
        parent=svg,
        name="green vertical",
        position=(260, 25),
        width=200,
        height=50,
        rotation=180,
    )
    osvg_collections.it.network.RouterBlue(
        parent=svg,
        name="Blue vertical",
        position=(240, 160),
        width=50,
        height=200,
    )
    osvg_collections.it.network.RouterOrange(
        parent=svg,
        name="Orange square",
        position=(320, 110),
        width=100,
        height=100,
    )
    osvg_collections.it.network.RouterOrange(
        parent=svg,
        name="Orange Mini horizontal",
        position=(320, 210),
        width=50,
        height=10,
    )
    return svg
