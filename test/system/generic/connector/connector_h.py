"""
Creating SVG object with horizontal connectors
"""

# pylint: disable=duplicate-code

import osvg
from osvg_collections.generic.connector import ConnectorH


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(
        width=200,
        height=160,
        style=osvg.Style(stroke_color="0000ff"),
    )
    osvg.Rectangle(
        parent=svg,
        width=1000,
        height=600,
        style=osvg.Style(stroke_width=0, fill_color="e0e0e0"),
    )
    r_style = osvg.Style(fill_color="a0a0a0", stroke_color="000000")
    r1 = osvg.Rectangle(
        parent=svg,
        name="top rectangle",
        position=(60, 25),
        width=100,
        height=40,
        style=r_style,
    )
    r2 = osvg.Rectangle(
        parent=svg,
        name="bottom rectangle",
        position=(90, 100),
        width=100,
        height=40,
        style=r_style,
    )
    c1 = ConnectorH(
        parent=svg, start=r1.connectors["bottom-left"], end=r2.connectors["top-left"]
    )
    c1.add_start_marker(marker_class=osvg.ArrowMarker)
    c2 = ConnectorH(
        parent=svg,
        start=r2.connectors["top-center"],
        end=r1.connectors["bottom-center"],
        absolute_y=140,
    )
    c2.add_end_marker(marker_class=osvg.ArrowMarker)
    ConnectorH(
        parent=svg,
        start=r1.connectors["bottom-right"],
        end=r2.connectors["top-right"],
        percentage=80,
    )
    return svg
