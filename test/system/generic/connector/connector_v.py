"""
Creating SVG object with vertical connectors
"""

# pylint: disable=duplicate-code

import osvg
from osvg_collections.generic.connector import ConnectorV


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(
        width=200,
        height=160,
        style=osvg.Style(stroke_color="0000ff"),
    )
    osvg.Rectangle(
        parent=svg,
        width=1000,
        height=600,
        style=osvg.Style(stroke_width=0, fill_color="e0e0e0"),
    )
    r_style = osvg.Style(fill_color="a0a0a0", stroke_color="000000")
    r1 = osvg.Rectangle(
        parent=svg,
        name="left rectangle",
        position=(25, 60),
        width=40,
        height=100,
        style=r_style,
    )
    r2 = osvg.Rectangle(
        parent=svg,
        name="right rectangle",
        position=(100, 90),
        width=40,
        height=100,
        style=r_style,
    )
    c1 = ConnectorV(
        parent=svg, start=r1.connectors["top-right"], end=r2.connectors["top-left"]
    )
    c1.add_start_marker(marker_class=osvg.ArrowMarker)
    c2 = ConnectorV(
        parent=svg,
        start=r2.connectors["center-left"],
        end=r1.connectors["center-right"],
        absolute_x=140,
    )
    c2.add_end_marker(marker_class=osvg.ArrowMarker)
    ConnectorV(
        parent=svg,
        start=r1.connectors["bottom-right"],
        end=r2.connectors["bottom-left"],
        percentage=80,
    )
    return svg
