"""
Creating SVG object with horizontal connectors
"""

# pylint: disable=duplicate-code

import osvg
from osvg_collections.generic.connector import MPConnectorH


def get_svg():
    """
    Create and return OSVG SVG object
    """
    svg = osvg.SVG(
        width=120,
        height=120,
        style=osvg.Style(stroke_color="ff0000", fill_color="ccffcc"),
    )
    osvg.Rectangle(
        parent=svg,
        width=1000,
        height=600,
        style=osvg.Style(stroke_width=0, fill_color="e0e0e0"),
    )
    ###
    ### 3 Point standard connector
    ###
    case_y = 0
    case_x = 0
    c1 = osvg.Circle(parent=svg, position=(case_x + 10, case_y + 10), radius=4)
    c2 = osvg.Circle(parent=svg, position=(case_x + 30, case_y + 10), radius=4)
    c3 = osvg.Circle(parent=svg, position=(case_x + 20, case_y + 30), radius=4)
    MPConnectorH(
        parent=svg,
        positions=[c.position for c in [c1, c2, c3]],
        style=osvg.Style(stroke_color="333333"),
    )
    ###
    ### 4 Point connector with percentage and arrow marker
    ###
    case_y = 0
    case_x = 50
    c1 = osvg.Circle(parent=svg, position=(case_x + 10, case_y + 10), radius=4)
    c2 = osvg.Circle(parent=svg, position=(case_x + 30, case_y + 10), radius=4)
    c3 = osvg.Circle(parent=svg, position=(case_x + 20, case_y + 30), radius=4)
    c4 = osvg.Circle(parent=svg, position=(case_x + 40, case_y + 55), radius=4)
    mp = MPConnectorH(
        parent=svg,
        positions=[c.position for c in [c1, c2]],
        percentage=65,
        style=osvg.Style(stroke_color="333333"),
    )
    mp.add_position((c3.position.x, c3.position.y))
    mp.add_marker(marker_class=osvg.ArrowMarker)
    mp.add_position(c4.position)
    ###
    ### 5 Point connector with absolute_y
    ###
    case_y = 60
    case_x = 0
    c1 = osvg.Circle(parent=svg, position=(case_x + 10, case_y + 10), radius=4)
    c2 = osvg.Circle(parent=svg, position=(case_x + 30, case_y + 10), radius=4)
    c3 = osvg.Circle(parent=svg, position=(case_x + 20, case_y + 30), radius=4)
    c4 = osvg.Circle(parent=svg, position=(case_x + 40, case_y + 20), radius=4)
    c5 = osvg.Circle(parent=svg, position=(case_x + 50, case_y + 25), radius=4)
    mp = MPConnectorH(
        parent=svg,
        positions=[c.position for c in [c1, c2, c3, c4, c5]],
        absolute_y=case_y + 45,
        style=osvg.Style(stroke_color="333333"),
    )
    mp.add_marker(marker_class=osvg.ArrowMarker)
    ###
    ### 5 Point connector with average
    ###
    case_y = 60
    case_x = 50
    c1 = osvg.Circle(parent=svg, position=(case_x + 10, case_y + 10), radius=4)
    c2 = osvg.Circle(parent=svg, position=(case_x + 30, case_y + 10), radius=4)
    c3 = osvg.Circle(parent=svg, position=(case_x + 20, case_y + 30), radius=4)
    c4 = osvg.Circle(parent=svg, position=(case_x + 40, case_y + 20), radius=4)
    c5 = osvg.Circle(parent=svg, position=(case_x + 50, case_y + 25), radius=4)
    mp = MPConnectorH(
        parent=svg,
        positions=[c.position for c in [c1, c2, c3, c4, c5]],
        average=True,
        style=osvg.Style(stroke_color="333333"),
    )
    return svg
