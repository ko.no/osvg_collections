"""
Creating SVG files to verify collection shapes.
"""

import os
import sys
import pathlib

sys.path.append(os.getcwd())

# pylint: disable=wrong-import-position
import test.system.generic.connector.connector_h
import test.system.generic.connector.connector_v
import test.system.generic.connector.connector_mp_h
import test.system.generic.connector.connector_mp_v
import test.system.it.network.clouds
import test.system.it.network.firewalls
import test.system.it.network.routers
import test.system.it.network.switches

PATH = "test/system"
changed_files = []


def check_and_write_xml_code(path, filename, svg_element):
    """
    Read current XML code, compare to intended and write intended XML code.
    """
    filename = filename + ".svg"
    path = os.path.join(PATH, path)
    os.makedirs(path, exist_ok=True)
    file_path = os.path.join(path, filename)
    if filename not in os.listdir(path):
        pathlib.Path(file_path).touch()
    with open(file_path, "r+", encoding="UTF-8") as f:
        old_content = f.read()
        new_content = svg_element.xml_string
        if old_content != new_content:
            changed_files.append(file_path)
        f.seek(0)
        f.write(svg_element.xml_string)
        f.truncate()
    print(f"File {file_path} written")


###
### generic.connector.connector_h
###
svg = test.system.generic.connector.connector_h.get_svg()
check_and_write_xml_code("generic/connector", "connector_h", svg)
###
### generic.connector.connector_v
###
svg = test.system.generic.connector.connector_v.get_svg()
check_and_write_xml_code("generic/connector", "connector_v", svg)
###
### generic.connector.connector_mp_h
###
svg = test.system.generic.connector.connector_mp_h.get_svg()
check_and_write_xml_code("generic/connector", "connector_mp_h", svg)
###
### generic.connector.connector_mp_v
###
svg = test.system.generic.connector.connector_mp_v.get_svg()
check_and_write_xml_code("generic/connector", "connector_mp_v", svg)
###
### it.network.router
###
svg = test.system.it.network.routers.get_svg()
check_and_write_xml_code("it/network", "routers", svg)
###
### it.network.switch
###
svg = test.system.it.network.switches.get_svg()
check_and_write_xml_code("it/network", "switches", svg)
###
### it.network.firewall
###
svg = test.system.it.network.firewalls.get_svg()
check_and_write_xml_code("it/network", "firewalls", svg)
###
### it.network.cloud
###
svg = test.system.it.network.clouds.get_svg()
check_and_write_xml_code("it/network", "clouds", svg)

if changed_files:
    print("CHANGED FILES:")
    for fname in changed_files:
        print(fname)
    sys.exit(1)

print("")
print("No changes detected.")
sys.exit(0)
