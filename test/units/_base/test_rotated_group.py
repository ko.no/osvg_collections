"""
Unit test for Rotated Group class
"""

# pylint: disable=missing-class-docstring,missing-function-docstring,
# pylint: disable=broad-exception-caught,protected-access,too-few-public-methods

import xml.etree.ElementTree as ET
import osvg_collections._base.rotated_group as T


class TestRotatedGroup:
    def test_add_element_rotation1(self):
        t = T.RotatedGroup()
        element = ET.Element("test_tag")
        t.add_element_rotation(element=element)
        assert element.keys() == []

    def test_add_element_rotation2(self):
        t = T.RotatedGroup(rotation=10)
        element = ET.Element("test_tag")
        t.add_element_rotation(element=element)
        assert element.keys() == ["transform"]
        assert element.get("transform") == "rotate(10 0 0)"
