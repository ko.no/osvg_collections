"""
Unit test for RectangleBasedShape Meta class
"""

# pylint: disable=missing-class-docstring,missing-function-docstring,
# pylint: disable=broad-exception-caught,protected-access,too-few-public-methods

import osvg
import osvg_collections._base.rectangle_shape as T


class TestRectangleBasedShape:
    class _T(osvg.Rectangle, T.RectangleBasedShape):
        border_color = "ffffff"

    def test_border_width(self):
        t = self._T(width=100, height=50)
        assert (
            float(t.border_width) == (100 + 50) / 100 * self._T.border_width_precentage
        )

    def test_inner_stroke_width(self):
        t = self._T(width=100, height=50)
        assert (
            float(t.inner_stroke_width)
            == min(100, 50) / 100 * self._T.border_width_precentage * 2
        )

    def test_fill_width(self):
        t = self._T(width=100, height=50)
        assert float(t.fill_width) == float(100 - t.border_width * 2)

    def test_fill_heigth(self):
        t = self._T(width=100, height=50)
        assert float(t.fill_heigth) == float(50 - t.border_width * 2)

    def test_get_border_rect(self):
        t = self._T(width=100, height=50)
        br = t.get_border_rectangle(None)
        assert float(br.width) == 100
        assert float(br.height) == 50
        assert br.style.attributes["fill_color"].value == "ffffff"

    def test_get_fill_rect(self):
        t = self._T(width=100, height=50)
        br = t.get_fill_rectangle(None)
        assert float(br.width) == float(100 - t.border_width * 2)
        assert float(br.height) == float(50 - t.border_width * 2)
        assert float(br.style.attributes["stroke_width"].value) == 0


class TestSCRectangleBasedShape:
    class _T(osvg.SCRectangle, T.SCRectangleBasedShape):
        border_color = "ffffff"

    def test_get_border_rect(self):
        t = self._T(width=100, height=50)
        br = t.get_border_rectangle(None)
        assert float(br.width) == 100
        assert float(br.height) == 50
        assert br.style.attributes["fill_color"].value == "ffffff"
        assert float(br.percentage) == float(t.border_width)

    def test_get_fill_rect(self):
        t = self._T(width=100, height=50)
        br = t.get_fill_rectangle(None)
        assert float(br.width) == float(100 - t.border_width * 2)
        assert float(br.height) == float(50 - t.border_width * 2)
        assert float(br.style.attributes["stroke_width"].value) == 0
        assert float(br.percentage) == float(
            (t.fill_width + t.fill_heigth) / 100 * t.border_width_precentage
        )
