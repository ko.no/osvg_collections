"""
Unit test for Multi-Point Connector classes
"""

# pylint: disable=missing-class-docstring,missing-function-docstring
# pylint: disable=broad-exception-caught,protected-access,too-few-public-methods

from test.units.helpers import XML_HEADER, etree_element_to_string
import osvg
import pytest
import osvg_collections.generic.connector.connector_mp as T


MidValuesTestData = [
    # args, exp_v1 , exp_v2
    ((1,), 1, 1),
    ((1, osvg.Float(2)), 1, 2),
    ((1, osvg.Float(2), 4), 1, 2),
    ((4, osvg.Float(2), -4), 2, 4),
    ((5, osvg.Float(2), 3, -4), 2, 3),
    ((1, 2, 3, 6, 7), 2, 3),
    ((0, 1, 2, 3, 6, 7), 2, 3),
    ((0, 1, 1, 2, 2, 7), 1, 2),
]


class TestMidValues:
    def test_no_values(self):
        try:
            T._two_middle_values()
        except Exception as e:
            assert isinstance(e, ValueError)
            assert str(e) == "Missing arguments"
        else:
            raise AssertionError("Should have raised an exception")

    @pytest.mark.parametrize("args, exp_v1 , exp_v2", MidValuesTestData)
    def test_values(self, args, exp_v1, exp_v2):
        v1, v2 = T._two_middle_values(*args)
        assert v1 == exp_v1
        assert v2 == exp_v2

    def test_generators(self):
        v1, v2 = T._two_middle_values(x for x in [1, 4, 5, 7])
        assert v1 == 4
        assert v2 == 5

    def test_list(self):
        v1, v2 = T._two_middle_values([1, 4, 5, 7])
        assert v1 == 4
        assert v2 == 5

    def test_tuple(self):
        v1, v2 = T._two_middle_values((1, 4, 5, 7))
        assert v1 == 4
        assert v2 == 5

    def test_mixed(self):
        v1, v2 = T._two_middle_values((1, 4), 5, [7, 7])
        assert v1 == 4
        assert v2 == 5


AverageValuesTestData = [
    # args, exp
    ((1,), 1),
    ((1, osvg.Float(2)), 1.5),
    ((osvg.Float(2), 1, 3), 2),
    ((-1, -1, 0, 1, 1), 0),
]


class TestMPConnectorH:
    def test_etree_element_basic1(self):
        p1 = osvg.Position(10, 20)
        p2 = osvg.Position(20, 30)
        t1 = T.MPConnectorH(positions=[p1, p2])
        t1.add_position((30, 20))
        assert etree_element_to_string(t1.etree_element) == XML_HEADER + [
            "<g",
            'points="10,20',
            "20,30",
            '30,20">',
            "<polyline",
            'points="8,25',
            '32,25"/>',
            "<polyline",
            'points="10,20',
            '10,25"/>',
            "<polyline",
            'points="20,30',
            '20,25"/>',
            "<polyline",
            'points="30,20',
            '30,25"/>',
            "</g>",
        ]


class TestMPConnectorV:
    def test_etree_element_basic1(self):
        p1 = osvg.Position(10, 20)
        p2 = osvg.Position(20, 30)
        t1 = T.MPConnectorV(positions=[p1, p2])
        t1.add_position((10, 40))
        assert etree_element_to_string(t1.etree_element) == XML_HEADER + [
            "<g",
            'points="10,20',
            "20,30",
            '10,40">',
            "<polyline",
            'points="15,18',
            '15,42"/>',
            "<polyline",
            'points="10,20',
            '15,20"/>',
            "<polyline",
            'points="20,30',
            '15,30"/>',
            "<polyline",
            'points="10,40',
            '15,40"/>',
            "</g>",
        ]
