"""
Unit test for Multi-Point Connector classes
"""

# pylint: disable=missing-class-docstring,missing-function-docstring
# pylint: disable=broad-exception-caught,protected-access,too-few-public-methods

from test.units.helpers import XML_HEADER, etree_element_to_string
import osvg
import osvg_collections.generic.connector.connector2p as T


class TestConnectorH:
    def test_etree_element_basic1(self):
        p1 = osvg.Position(10, 20)
        p2 = osvg.Position(20, 30)
        t1 = T.ConnectorH(start=p1, end=p2)
        assert etree_element_to_string(t1.etree_element) == XML_HEADER + [
            "<g",
            'points="10,20',
            '20,30">',
            "<polyline",
            'style="fill:none"',
            'points="10,20',
            "10,25",
            "20,25",
            '20,30"/>',
            "</g>",
        ]


class TestConnectorV:
    def test_etree_element_basic1(self):
        p1 = osvg.Position(10, 20)
        p2 = osvg.Position(20, 30)
        t1 = T.ConnectorV(start=p1, end=p2)
        assert etree_element_to_string(t1.etree_element) == XML_HEADER + [
            "<g",
            'points="10,20',
            '20,30">',
            "<polyline",
            'style="fill:none"',
            'points="10,20',
            "15,20",
            "15,30",
            '20,30"/>',
            "</g>",
        ]
