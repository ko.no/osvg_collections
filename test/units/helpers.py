"""
Unit test helpers
"""

import xml.dom.minidom
import xml.etree.ElementTree as ET

XML_HEADER = [
    "<?xml",
    'version="1.0"',
    "?>",
]


def etree_element_to_string(element):
    """
    Parse Element Tree to pretty text snippets.
    """
    return xml.dom.minidom.parseString(ET.tostring(element)).toprettyxml().split()
